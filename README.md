# Stalker Portal Sync Tool
Keep multiple portals content in sync.

The script allows to get exactly the same channels, VOD movies, tariff plans and EPG data in multiple portals.

(To be in sync means the data to be the same as at the parent node)

## Installation
* Select one of the portals as the parent node
* Create MySQL user username@parent.host at each child node
* Download and extract the release archive to the parent node
* Rename ```example.config.php``` to ```config.php```

## Config file contents
* ```parent``` - parent portal database credits
* ```children``` - child nodes' database credits
* ```methods``` - available methods to sync:
  * ```itv``` -  TV channels
  * ```vod``` - Videoclub
  * ```tariffs``` - tariff plans and services packages
  * ```epg``` - EPG data
  * ```--all-methods``` - list the methods to sync in bulk

## Settings
```sync.php``` actually calls the ```universalTableSync()``` function for each database table related to the specified method.
Comment the line that calls the table you don't want to be synchronized.

## Usage
The script should be run like ```php sync.php <child_name> <method>```

```--all-portals``` option as a child_name syncs all children

```--all-methods``` option as the method does the same for the methods list

## Examples
That's how it would go for the example.config.php:

```php sync.php child_2 itv``` - TV channels will be synchronized between parent and child_2 nodes

```php sync.php --all-portals vod``` - Videoclub movies will be syncronized between all portals.

```php sync.php --all-portals --all-methods``` - TV archive and Videoclub will be synchronized between all portals
