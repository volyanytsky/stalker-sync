<?php

return [

  'parent' => [
    "type"        => "mysql",
    "host"        => "localhost",
    "name"        => "stalker_db",
    "user"        => "stalker",
    "pass"        => "1",
  ],

  'children' => [

    'child_1' => [
      "type"        => "mysql",
      "host"        => "10.118.41.10",
      "name"        => "stalker_db",
      "user"        => "stalker",
      "pass"        => "1",
    ],
    'child_2' => [
      "type"        => "mysql",
      "host"        => "10.118.41.11",
      "name"        => "stalker_db",
      "user"        => "stalker",
      "pass"        => "1",
    ],
    'child_3' => [
      "type"        => "mysql",
      "host"        => "10.118.41.12",
      "name"        => "stalker_db",
      "user"        => "stalker",
      "pass"        => "1",
    ],

  ],

  'methods' => [
    'itv',
    'vod',
    'tariffs',
    'epg',
    '--all-methods' => [
      'itv',
      'vod',
    ],
  ],

];

 ?>
