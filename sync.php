<?php

if(!isset($argv[1], $argv[2]))
{
  exit("Undefined child or sync method \n");
}

require_once 'functions.php';
$config = require 'config.php';
checkConfigIntegrity($config);

if(!in_array($argv[2], $config['methods']))
{
  exit("Wrong sync method\n");
}

$child = $argv[1];
$method = $argv[2];

$parent = $config['parent'];
$children = null;

switch($child)
{
  case (isset($config['children'][$child]) ? true : false):
    $children[$child] = $config['children'][$child];
    break;
  case '--all-portals':
    $children = $config['children'];
    break;
  default:
    exit("Wrong child name\n");
}

databaseCreditsIntegrity('parent', $parent);
$pdoParent = pdoConnect($parent);

foreach ($children as $name => $databaseCreds)
{
  echo "sync $name child\n";

  databaseCreditsIntegrity($name, $databaseCreds);
  $pdoChild = pdoConnect($databaseCreds);

  if( $method == "itv" || ($method == '--all-methods' && in_array('itv', $config['--all-methods'])))
  {
    echo "sync tv channels:\n";

    echo "\t-itv\n";
    universalTableSync($pdoParent, $pdoChild, "itv");

    echo "\t-ch_links\n";
    universalTableSync($pdoParent, $pdoChild, "ch_links");

    //echo "\t-stream_zones\n";
    //universalTableSync($pdoParent, $pdoChild, "stream_zones");

    echo "\t-streaming_servers\n";
    universalTableSync($pdoParent, $pdoChild, "streaming_servers", "id", ["id", "status", "stream_zone"]);

    echo "\t-storages\n";
    universalTableSync($pdoParent, $pdoChild, "storages", "id", ["id", "apache_port"]);

    echo "\t-ch_link_on_streamer\n";
    universalTableSync($pdoParent, $pdoChild, "ch_link_on_streamer");

    //echo "\t-countries_in_zone\n";
    //universalTableSync($pdoParent, $pdoChild, "countries_in_zone");

    echo "\t-tv_archive\n";
    universalTableSync($pdoParent, $pdoChild, "tv_archive");

    echo "\t-tv_genre\n";
    universalTableSync($pdoParent, $pdoChild, "tv_genre");

    echo "setting emulated child storages\n";
    $storageEmulStn = $pdoChild->prepare("UPDATE storages SET fake_tv_archive = 1");
    $storageEmulStn->execute();
  }

  if( $method == "tariffs" || ($method == '--all-methods' && in_array('tariffs', $config['--all-methods'])))
  {
    echo "sync tariff plans\n";

    echo "\t-services_package\n";
    universalTableSync($pdoParent, $pdoChild, "services_package");

    echo "\t-service_in_package\n";
    universalTableSync($pdoParent, $pdoChild, "service_in_package");

    echo "\t-tariff_plan\n";
    universalTableSync($pdoParent, $pdoChild, "tariff_plan");

    echo "\t-package_in_plan\n";
    universalTableSync($pdoParent, $pdoChild, "package_in_plan");
  }

  if( $method == "vod" || ($method == '--all-methods' && in_array('vod', $config['--all-methods'])))
  {
    echo "sync videoclub\n";

    echo "\t-media_category\n";
    universalTableSync($pdoParent, $pdoChild, "media_category");

    echo "\t-genre\n";
    universalTableSync($pdoParent, $pdoChild, "genre");

    echo "\t-cat_genre\n";
    universalTableSync($pdoParent, $pdoChild, "cat_genre");

    echo "\t-video\n";
    universalTableSync($pdoParent, $pdoChild, "video");

    echo "\t-video_season\n";
    universalTableSync($pdoParent, $pdoChild, "video_season");

    echo "\t-video_season_series\n";
    universalTableSync($pdoParent, $pdoChild, "video_season_series");

    echo "\t-video_series_files\n";
    universalTableSync($pdoParent, $pdoChild, "video_series_files");

    echo "\t-screenshots\n";
    universalTableSync($pdoParent, $pdoChild, "screenshots");
  }

  if( $method == "epg" || ($method == '--all-methods' && in_array('epg', $config['--all-methods'])))
  {
    echo "sync epg\n";

    echo "\t-screenshots\n";
    universalTableSync($pdoParent, $pdoChild, "epg_setting", "id", ["etag", "updated", "id_prefix", "status"]);
  }

  echo "child $name sync finished\n";
}

echo "sync finished\n";

exit;

 ?>
