<?php

function databaseCreditsIntegrity($name, array &$creds)
{
  $keys = ['type', 'host', 'name', 'user', 'pass'];
  foreach ($keys as $key)
  {
    if(!isset($creds[$key]))
    {
      throw new \Exception("$name portal not set database $key", 1);
    }
  }
}

function checkConfigIntegrity(array &$config)
{
  $sections = ['parent', 'children', 'methods'];
  foreach ($sections as $section)
  {
    if(!isset($config[$section]) || !is_array($config[$section]))
    {
      throw new \Exception("config[$section] is not set", 1);
    }
  }

  databaseCreditsIntegrity('parent', $config['parent']);
  foreach ($config['children'] as $child => $creds)
  {
    databaseCreditsIntegrity($child, $creds);
  }
}

function pdoConnect(&$db)
{
  $keys =
  $options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
  ];
  return new PDO($db['type'] . ":host=" . $db['host'] . ";dbname=" . $db['name'].";charset=utf8", $db['user'], $db['pass'], $options);
}

function pdoFetchAll(PDO $pdo, $query, $args = null)
{
  $request = $pdo->prepare($query);
  $request->execute($args);
  return $request->fetchAll();
}

function delete(PDO $pdo, $table, $column, $value)
{
  $sql = "DELETE FROM $table WHERE $column = '$value'";
  $stn = $pdo->prepare($sql);
  $stn->execute();
}

function prepareInsOnDuplUpdSqlString($table, array $columns, array $keys)
{
  $fields = $columns;
  $insert = implode(",",$fields);
  $sql = "INSERT INTO `$table`($insert) VALUES(";
  foreach($fields as $column)
  {
    $sql .= ":".$column.",";
  }
  $sql = rtrim($sql, ",") . ") ON DUPLICATE KEY UPDATE ";
  foreach($fields as $column)
  {
    if(!in_array($column, $keys))
    {
      $sql .= "$column=VALUES($column),";
    }
  }
  return rtrim($sql,",");
}

function getTableFields(PDO $pdo, $table)
{
  $data = pdoFetchAll($pdo, 'DESC ' . $table);
  return array_column($data, 'Field');
}

function universalTableSync(PDO $parentPdo, PDO $childPdo, $table, $key = 'id', $notUpdate = ['id'])
{
  $parentSelection = pdoFetchAll($parentPdo, "SELECT * FROM $table");

  $childKeys = getTableFields($childPdo, $table);

  foreach($parentSelection as $row)
  {
    foreach($row as $field => $value)
    {
        if(!in_array($field, $childKeys))
        {
          unset($row[$field]);
        }
    }
  }

  $parentSelectionKeyList = array_column($parentSelection, $key);

    $sql = prepareInsOnDuplUpdSqlString($table, $childKeys, $notUpdate);
    $stn = $childPdo->prepare($sql);
    $childKeySelection = pdoFetchAll($childPdo, "SELECT $key FROM $table");
    $childKeyList = array_column($childKeySelection, $key);
    foreach($childKeyList as $keyValue)
    {
      if(!in_array($keyValue, $parentSelectionKeyList))
      {
        delete($childPdo, $table, $key, $keyValue);
      }
    }

    $childFields = getTableFields($childPdo, $table);
    foreach($parentSelection as $row)
    {
      $values = [];
      foreach($row as $column => $value)
      {
        if(in_array($column, $childFields))
        {
          ($value) ? $values[":".$column] = $value : $values[":".$column] = "";
        }
      }
      $stn->execute($values);
    }
}

 ?>
